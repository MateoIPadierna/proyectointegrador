# coding=utf-8
"""
Archivo Utilidades con métodos para:
- Realizar peticion de los datosMateriasEstudiantes a los respectivos webservices
- Procesar la info de la peticion para dejarla lista para el modelo en la BD
-
"""
import requests
import json
import datetime
from .models import Profesor, HistorialMaterias, Materia, MateriasDisponibles, ClasesProfesor, Clase
from suds.client import Client
from suds.sudsobject import asdict
import suds
import ast
import random

caracteresReemplazo = {}
caracteresReemplazo['\xd3'] = 'O'
caracteresReemplazo['\xc1'] = 'A'
caracteresReemplazo['\xcd'] = 'I'
caracteresReemplazo['\xd2'] = 'O'
caracteresReemplazo['\xc8'] = 'E'
caracteresReemplazo['\xda'] = 'U'
caracteresReemplazo['\xc0'] = 'A'
caracteresReemplazo['\xcc'] = 'I'


def obtenerCliente():
    client = Client('http://acadtest.ucaldas.edu.co:8084/WsfacIngenieria.asmx?WSDL',
                    headers={'Content-Type': 'text/xml; charset=utf-8'})
    return client


def peticionDatosWS():
    # pip install suds-jurko --> comando para instalar la libreria para consulta

    client = obtenerCliente()

    ##La salida de c/servicio Arreglo de diccionarios
    # getEstudiante = peticionGetEstudiante(client, '1701313741')
    # getestudiantes = peticionGetEstudiantes(client, '2017', '2', '170')
    # getEstudiantesGrupo=peticionGetEstudiantesGrupo(client, '2017', '2', '170')
    # getGrupos = peticionGetGrupos(client, '2017', '2', 'G8F0090', '170')  # G8F0090,G8F0102, G8F0152
    # getHorarios=peticionGetHorarios(client,'2017','2')


    # listaHistorialMaterias = peticionGetGrupoSesion(client, '2016', '1', 'G8F0047')  ##Historial de una sola materia
    # insercionesHistorialMateria(listaHistorialMaterias)


    # Lineas para la insercion e reistros en las tablas Profesores, HistorialMateria y MateriasDIsponibles
    # getMaterias = peticionGetMaterias(client, '2017', '2', '170')
    # insercionTablaHistorialMaterias(getMaterias,client)
    #
    # listaProfes = peticionGetProfesores(client, '2017', '2', '170')
    # insercionesTablaProfesor(listaProfes)

    # datosMateriasDisp = peticionDatosWSLocal()
    # insercionesTablaMateriasDisponibles(datosMateriasDisp)


def obtenerPeriodoActual():
    f = datetime.date.today()
    periodo = 1
    año = 2018

    # año = f.year
    # if f.month <= 6:
    #     periodo = 1
    # else:
    #     periodo = 2
    return (año, periodo)


def peticionsita(codigo):
    client = obtenerCliente()
    result = client.service.getEstudiantePensum(
        codigo=codigo
    )
    r = result[26:-2]
    dic = ast.literal_eval(r)
    for d in dic:
        print(str(d))
    return dic


def peticionGetEstudiante(cliente, codigo):
    result = cliente.service.getEstudiantePensum(
        codigo=codigo
    )
    r = result[26:-2]
    dic = ast.literal_eval(r)
    for d in dic:
        print(str(d))
    return dic


def peticionGetEstudiantes(cliente, año, periodo, codCarrera):
    result = cliente.service.getEstudiantes(
        ano=año,
        periodo=periodo,
        cod_carrera=codCarrera
    )
    r = result[21:-2]
    l = cadenaADiccionario(r)
    return l


def peticionGetEstudiantesGrupo(cliente, año, periodo, codCarrera):
    result = cliente.service.getEstudiantesGrupo(
        ano=año,
        periodo=periodo,
        cod_carrera=codCarrera
    )
    r = result[19:-2]
    l = cadenaADiccionario(r)
    return l


def peticionGetGrupoSesion(cliente, año, periodo, codMateria):
    try:
        result = cliente.service.getGrupoSesion(
            ano=año,
            periodo=periodo,
            cod_materia=codMateria
        )

        r = result[17:-2]
        cadena = ""
        # print(str(result))
        for c in range(len(r)):
            caracter = r[c]
            if caracter == '[':
                caracter = '"' + caracter
            elif caracter == ']':
                caracter = caracter + '"'
            cadena = cadena + caracter
        l = cadenaADiccionario(cadena)
        return l
    except suds.WebFault:
        print('Error peticion materia ' + año + "-" + periodo + " " + codMateria)
        return []


def peticionGetGrupos(cliente, año, periodo, codMateria, codCarrera):
    result = cliente.service.getGrupos(
        ano=año,
        periodo=periodo,
        cod_materia=codMateria,
        cod_carrera=codCarrera
    )
    r = result[15:-2]
    print("RRR_" + result)
    l = cadenaADiccionario(r)
    return l


def peticionGetHorarios(cliente, año, periodo):
    result = cliente.service.getHorarios(
        ano=año,
        periodo=periodo
    )
    r = result[17:-2]
    # print(r.encode('utf-8'))
    l = cadenaADiccionario(r)
    return l


def peticionGetMaterias(cliente, año, periodo, codCarrera):
    result = cliente.service.getMaterias(
        ano=año,
        periodo=periodo,
        cod_carrera=codCarrera
    )
    r = result[18:-2]
    # print(r.encode('utf-8'))
    l = cadenaADiccionario(r)
    return filtrarMateriasPrograma(l)


def peticionGetProfesores(cliente, año, periodo, codCarrera):
    result = cliente.service.getProfesores(
        ano=año,
        periodo=periodo,
        cod_carrera=codCarrera
    )
    r = result[20:-2]
    # print(r.encode('utf-8'))
    l = cadenaADiccionario(r)

    return l


def cadenaADiccionario(resultado):
    global caracteresReemplazo
    lista = []
    cadena = ""
    # print(r.encode('utf-8'))
    for c in range(len(resultado)):
        caracter = resultado[c]

        if caracter in caracteresReemplazo.keys():
            caracter = caracteresReemplazo[caracter]

        cadena = cadena + caracter
        if caracter == '}':
            if cadena[0] == ',':
                cadena = cadena[1:]

            # print(cadena)
            dic = ast.literal_eval(cadena)
            lista.append(dic)
            # print(" dicc " + str(dic))
            cadena = ""
    return lista


def peticionDatosWSLocal():
    """
        Método que se utiliza para realizar la petición
        y retornar los datosMateriasEstudiantes en formato json para ser usados
        en los metodos de contexto
        :return: datosMateriasEstudiantes en formato JSON
    """

    data_headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

    try:
        post = requests.post('http://localhost:8000/classTeacher/json/WSmxe/', headers=data_headers)
        data = post.json()
        registrosMateriasDisp = json.loads(data)

    except ConnectionError as e:
        print("CONNECTION ERROR: ")
        print(e)

    # datosMateriasEstudiantes = requests.get('http://localhost:8000/classTeacher/json/WSmxe/')
    # datosMateriasEstudiantes = datosMateriasEstudiantes.json()
    # datosMateriasEstudiantes = json.loads(datosMateriasEstudiantes)
    #
    # print("Datos peticion: "+str(datosMateriasEstudiantes))
    #
    # datosHistorialMateria = requests.get(
    #     "http://localhost:8000/classTeacher/json/WShxm")
    # datosHistorialMateria = datosHistorialMateria.json()
    # datosHistorialMateria = json.loads(datosHistorialMateria)
    #
    # datosDocente = requests.get(
    #     "http://localhost:8000/classTeacher/json/WSinfod")
    # datosDocente = datosDocente.json()
    # datosDocente = json.loads(datosDocente)

    # return datosMateriasEstudiantes, datosHistorialMateria, datosDocente
    return registrosMateriasDisp


def materiaPrograma(materia):
    materiasBD = Materia.objects.values_list('codigo', flat=True)
    if materia['COD_MATERIA'] in materiasBD:
        return True
    else:
        return False


def filtrarMateriasPrograma(materiasWS):
    materiasFiltradas = []
    materiasFiltradas = [x for x in materiasWS if materiaPrograma(x)]
    return materiasFiltradas


def insercionTablaHistorialMaterias(listaMaterias, client, año, periodo):
    listaMateriasError = ['G6K0801']
    for m in listaMaterias:
        if not (m['COD_MATERIA'] in listaMateriasError):
            print(str(m))
            hxm = peticionGetGrupoSesion(client, año, periodo, m['COD_MATERIA'])
            insercionesHistorialMateria(hxm)


def insercionesHistorialMateria(historialMaterias):
    for fila in historialMaterias:
        try:

            materia = Materia.objects.get(codigo=fila['COD_MATERIA'])
            hm = HistorialMaterias(periodo=fila['ANO'] + "-" + fila['PERIODO'], materia=materia,
                                   profesor=fila['NOMBRE'], grupo=fila['GRUPO'], inscritos=fila['INSCRITOS'],
                                   cancelaciones=fila['CANCELACIONES'], horario=fila['SESIONES']
                                   ##Este WS tiene la informacion del horario asi fila['SESION']=[Mi 14-2 Sala F][Lu 10-2 Sala E]
                                   )
            hm.save()
        except Exception:  ##La excecpion sale cuando algun campo imcumple la restriccion unique del modelo HistorialMaterias
            # print("\nExcp "+str(fila)+"\n")
            pass


def insercionesTablaMateriasDisponibles(registrosMateriasDisp):
    # print(str(registrosMateriasDisp))
    for fila in registrosMateriasDisp:

        for materia in fila['Materias Disponibles'].keys():
            materiaDisp = None
            try:
                materiaDisp = MateriasDisponibles.objects.get(materia=materia, estudiante=fila['Estudiante'])
                materiaDisp.veces_cursada = fila['Materias Disponibles'][materia]
                materiaDisp.save()
            except MateriasDisponibles.DoesNotExist:
                try:
                    materiaBD = Materia.objects.get(codigo=materia)
                    materiaDisp = MateriasDisponibles(estudiante=fila['Estudiante'], materia=materiaBD,
                                                      veces_cursada=fila['Materias Disponibles'][materia])
                    materiaDisp.save()
                except Materia.DoesNotExist:
                    print('No encuentra ' + str(materia))


def insercionesTablaProfesor(listaProfes):
    restricciones = ['Martes 12-2; Jueves 13-1', 'Lunes 12-2', 'Viernes 10-1; Sabado 8-4', 'Lunes 12-2; Lunes 6-2',
                     'Miercoles 12-3', 'Martes 12-2; Jueves 13-1', 'Lunes 12-2; Viernes 10-1; Sabado 8-4',
                     'Lunes 12-2; Miercoles 12-3',
                     'Lunes 7-2; Lunes 13-1', 'Lunes 12-2; Viernes 10-1', 'Lunes 12-2; Lunes 18-2', 'Miercoles 10-2',
                     'Miercoles 12-1; Jueves 13-1', ' Viernes 10-1; Jueves 8-4', 'Lunes 14-1; Miercoles 12-3']

    for fila in listaProfes:
        try:
            p = Profesor(codigo=fila['CEDULA'], nombre=fila['NOMBRES'],
                         apellido=fila['P_APELLIDO'] + " " + fila['S_APELLIDO'], correo=fila['EMAIL'],
                         carga=random.randint(10, 20), restricciones_horario=random.choice(restricciones))
            p.save()
            # print(str(len(random.choice(restricciones))))
        except ValueError:  ##La excepcion sale cuando la cedula del profe no es # entero
            pass


def modificarListaProfesor(lPro):
    listaProfes=[]
    for i in lPro:
        listaProfes.append((i[0],i[2]+" "+i[1]))
    return  listaProfes

def obtenerCodigoP(nombreP,listaP):
    for i in listaP:
        if i[1]==nombreP:
            return i[0]


def insercionesTablaClasesProfesor():


    pe = HistorialMaterias.objects.order_by('periodo').last()


    if pe!= None:
        hm = HistorialMaterias.objects.filter(periodo=pe.periodo)
        profes=Profesor.objects.values_list('codigo','nombre','apellido')
        lProfes=modificarListaProfesor(profes)
        for i in hm:
            if i.profesor.upper() != ' POR ASIGNAR  DOCENTE':
                #print(str(i.profesor)+" : "+str(i.materia_id)+" , "+str(obtenerCodigoP(i.profesor,lProfes)))
                try:
                    p=Profesor.objects.get(codigo=obtenerCodigoP(i.profesor,lProfes))
                    m=Materia.objects.get(codigo=i.materia_id)
                    c=Clase(grupo=0,duracion=0,materia_id=i.materia_id,profesor_id=obtenerCodigoP(i.profesor,lProfes),horario='')
                    c.save()
                except Profesor.DoesNotExist:
                    pass
            #     hm = HistorialMaterias.objects.filter(profesor=(i[2] + " " + i[1])).first()
            #     print("Si " + str(hm))
            #     c += 1
            # except HistorialMaterias.DoesNotExist:
            #     print("Nada " + (i[2] + " " + i[1]))
            #     c -=1

    #print("a "+str(c))

            # p = HistorialMaterias.objects.order_by('periodo').last()
            # if p!= None:
            #     historialPeriodo=HistorialMaterias.objects.filter(periodo=p.periodo)
            #     for hp in historialPeriodo:
            #         if hp.profesor.upper() != ' POR ASIGNAR  DOCENTE':
            #             pass
            #             #prof=Profesor.objects.get()
            #             #mat = Materia.objects.get(codigo=hp.materia_id)
            #             #cp=ClasesProfesor(profesor=hp.profesor,materia=hp.materia_id)
            #             #print(str(cp))
            #             #cp.save()
