# coding=utf-8
"""
Archivo proyeccion con métodos para:
- Recorrer el historico del ultimo periodo y contar cancelaciones por materia
-
"""
from .models import Materia, HistorialMaterias, Proyeccion
import datetime
from .utilidades import *
import os
import math


def proyeccion():
    resultadoProyeccion = {}

    listaEstudiantes = obtenerListadoEstudiantes()
    promHistorico = promedioInscritosHistorial()
    materias = Materia.objects.values('codigo', 'semestre', 'prioridad')
    for m in materias:
        materiaProyeccion = {}
        cantEstudiantes = cantEstudiantesMateria(m, listaEstudiantes)

        if m['codigo'] in promHistorico.keys():
            materiaProyeccion['cantEstudiantes'] = cantEstudiantes
            materiaProyeccion['promHistorica'] = promHistorico[m['codigo']]
            materiaProyeccion['codMateria'] = m['codigo']
            insertarFilaProyeccion(materiaProyeccion)

        difHistorico(m, cantEstudiantes, promHistorico)



        # cantidadEstudiantes = estudiantesXMateria(m.codigo, listaEstudiantes, materias)
        # cantidadAjustada = contrasteHistorico(codigo, cantidadEstudiantes, historico)
        # resultadoProyeccion[codigo] = cantidadAjustada

    return resultadoProyeccion


def difHistorico(materia, cantEstudiantes, promHistorico):
    if materia['codigo'] in promHistorico.keys():
        print("Prom H " + str(promHistorico[materia['codigo']]) + " m " + materia['codigo'] + " cant Est " + str(
            cantEstudiantes))
        ##--->Completar<---


def obtenerListadoEstudiantes():
    ##info del JSON
    pwd = os.path.dirname(__file__)
    archivo = open(pwd + '/static/classTeacher/JSONData/materiasDisponibles.json')
    data1 = json.load(archivo)
    dataJSON = json.dumps(data1)
    archivo.close()
    infoEstudiantes = json.loads(dataJSON)

    ##Info (menos el semestre) de la BD
    # estudiantes=[]
    # estBD = MateriasDisponibles.objects.values('estudiante', 'veces_cursada', 'materia_id')
    # for e in estBD:



    return infoEstudiantes


def insertarFilaProyeccion(materia):
    try:
        cupoMateria = Materia.objects.get(codigo=materia['codMateria'])
        grupos = materia['cantEstudiantes'] / cupoMateria.cupo
        grupos = math.ceil(grupos)
        periodo = obtenerPeriodoActual()
        filaProy = Proyeccion(materia=cupoMateria, cantidad_grupos=grupos,
                              cantidad_estudiantes=materia['cantEstudiantes'],
                              promedio_historico=materia['promHistorica'],
                              periodo=str(periodo[0]) + "-" + str(periodo[1]))
        filaProy.save()
    except Exception:  ##La excecpion sale cuando algun campo imcumple la restriccion unique del modelo

        pass


def cantEstudiantesMateria(materia, estudiantes):
    cupos = 0
    # print(str(estudiantes))
    for est in estudiantes:
        semestre = est['Semestre']
        if materia['codigo'] in est['Materias Disponibles']:
            rangS = rangoSemestre(materia['semestre'], semestre)
            rangR = rangoRepite(est['Materias Disponibles'], materia['codigo'])
            rangP = float(materia['prioridad'])
            sumRangos = rangS + rangR + rangP

            r = sumRangos * 5 / 3
            # print(str(sumRangos)+" r "+str(r))
            if r > 2:
                cupos += 1
            elif 1 <= r <= 2:
                cupos += 0.5
    return cupos
    # print(str(est['Materias Disponibles'])+" "+str(materia['codigo'])+" R "+str(rangR))


def rangoSemestre(semestreMateria, semestreEst):
    sM = 0
    if len(semestreMateria) == 2:
        sM = int(semestreMateria[0])
    elif len(semestreMateria) == 1:
        sM = int(semestreMateria)
    elif len(semestreMateria) == 3:
        sM = int(semestreMateria[2])

    if semestreEst - sM >= 2:
        return 1
    elif semestreEst - sM == 1:
        return 0.5
    else:
        return 0


def rangoRepite(materiasEst, codigoMateria):
    if materiasEst[codigoMateria] == 0:
        return 0
    elif materiasEst[codigoMateria] == 1:
        return 0.5
    elif materiasEst[codigoMateria] >= 2:
        return 1


def obtenerGruposPeriodo(periodo, codigoMateria):
    grupos = []
    for m in periodo['Materias']:
        l = list(m.keys())
        if codigoMateria in l:
            grupos.append(m[l[0]])
            # print("grupo "+codigoMateria+" : "+str(m[l[0]]))
    return grupos


def promediar(cantidad, arrHistorico):
    pass


def iniciarProyeccion(historico, materiasDisponibles):
    # cuposC = cuposXCancelacion(historico)
    # cuposR = cuposRepitentes(materiasDisponibles)
    # baseCuposMateria = sumaCuposRepiCanc(cuposR, cuposC)
    proyeccion()

    # obtenerHistorico4Semestres()  # Mira la BD y actualiza el historial de materias segun haga falta
    # promedioInscritosHistorial()#Promedio de inscritos x materia, segun datos de BD


def promedioInscritosHistorial():
    inscritosXMateria = {}
    periodos = valorUltimos4Periodos()
    promInscMateria = {}

    for p in periodos:
        mxp = HistorialMaterias.objects.filter(periodo=str(p[0]) + "-" + str(p[1]))
        for m in mxp:
            if m.materia_id not in inscritosXMateria:
                d = {}
                d[m.periodo] = m.inscritos
                inscritosXMateria[m.materia_id] = d
            else:
                d = inscritosXMateria[m.materia_id]
                if m.periodo not in d:
                    d[m.periodo] = m.inscritos
                else:
                    d[m.periodo] += m.inscritos

    for im in inscritosXMateria:
        # print(str(im) + " --> " + str(inscritosXMateria[im]))
        promInscMateria[im] = promediarMateria(inscritosXMateria[im])
    return promInscMateria


def promediarMateria(inscritosMateria):
    sum = 0
    for p in inscritosMateria.keys():
        sum += inscritosMateria[p]
    return sum / len(inscritosMateria.keys())


def obtenerHistorico4Semestres():
    periodosHistorico = valorUltimos4Periodos()
    periodosWS = periodosAConsultarWS(periodosHistorico)
    for p in periodosWS:
        print("\nMaterias Periodo" + str(p))
        c = obtenerCliente()
        materiasP = peticionGetMaterias(c, str(p[0]), str(p[1]), '170')
        # print("L" + str(len(materiasP)))
        insercionTablaHistorialMaterias(materiasP, c, str(p[0]), str(p[1]))


def valorUltimos4Periodos():
    pA = obtenerPeriodoActual()
    periodo = pA[1]
    año = pA[0]
    periodosHistorico = [None] * 4

    for i in range(4):
        if periodo == 2:
            periodo -= 1
        else:
            periodo += 1
            año -= 1
        periodosHistorico[i] = (año, periodo)
    return periodosHistorico


def periodosAConsultarWS(periodosHistorico):
    periodosPeticionWS = []
    p = HistorialMaterias.objects.order_by('periodo').first()
    if not (p == None):
        l = p.periodo.split("-")
        l = (int(l[0]), int(l[1]))
        # print("4 ultimos Peridos: " + str(periodosHistorico) + " Mas antiguo periodo BD " + str(l))
        if l in periodosHistorico:
            periodosPeticionWS = periodosHistorico[(periodosHistorico.index(l)) + 1:4]
        else:
            periodosPeticionWS = periodosHistorico
    else:
        periodosPeticionWS = periodosHistorico
    return periodosPeticionWS







    # def cuposRepitentes(materiasXEstudiantes):
    #     """
    #     :param materiasXEstudiantes: diccionario con datos de las materias que c/estudiante puede ver
    #     :return:diccionario con una llave por cada materia y con valor el numero de estan repitiendo la materia
    #     """
    #
    #     cupoRepitentesXMateria = {}
    #     for estudiante in materiasXEstudiantes:
    #         materias = estudiante["Materias Disponibles"]
    #         for m in materias:
    #             if m not in cupoRepitentesXMateria and materias[m] > 0:
    #                 cupoRepitentesXMateria[m] = materias[m]
    #             elif materias[m] > 0:
    #                 cupoRepitentesXMateria[m] += materias[m]
    #
    #     return cupoRepitentesXMateria
    #
    #
    # def sumaCuposRepiCanc(cuposRepi, cuposCanc):
    #     """
    #     :param cuposRepi: diccionario con valores de cupos por repitentes de c/materia
    #     :param cuposCanc: diccionario con valores de cupos por cancelaciones de c/materia
    #     :return: diccionario con una llave por cada materia y con valor el numero de estudiantes que cancelaron la materia
    #             en el semestre inmediatamente anterior + numero de estudiantes que la repiten
    #     """
    #     cuposBase = {}
    #
    #     for cr in cuposRepi.keys():
    #         cuposBase[cr] = cuposRepi[cr]
    #     for cc in cuposCanc.keys():
    #         if cc in cuposBase:
    #             cuposBase[cc] += cuposCanc[cc]
    #         else:
    #             cuposBase[cc] = cuposCanc[cc]
    #
    #     return cuposBase
    #
    #
    # def cuposXCancelacion(historico):
    #     """
    #     :param historico: diccionario con datos del historico de materias en los ultimos 4 periodos academicos
    #     :return: diccionario con una llave por cada materia y con valor el numero de estudiantes que cancelaron la materia
    #             en el semestre inmediatamente anterior
    #     """
    #     cuposCancelacionMaterias = {}
    #     ultimoPeriodo = historico[0]
    #     listaMaterias = ultimoPeriodo['Materias']
    #
    #     for m in listaMaterias:
    #         for c in m.keys():
    #             if c not in cuposCancelacionMaterias:
    #                 cuposCancelacionMaterias[c] = m[c]['Cancelaciones']
    #             else:
    #                 cuposCancelacionMaterias[c] += m[c]['Cancelaciones']
    #
    #     return cuposCancelacionMaterias




    # def estudiantesXMateria(codigo, estudiantes, materias):
    #     cupos = 0
    #     for est in estudiantes:
    #         semestre = est['Semestre']
    #         if codigo in estudiantes[est].keys():
    #             if estudiantes[est][codigo] > 0:
    #                 cupos += 1
    #             else:
    #                 if semestre >= materias[codigo]['MaxSemestre'] and materias[codigo][
    #                     'prioridad'] == 1:  ## --> asumiendo que materias es un diccionario con entradas de max semestre y prioridad
    #                     cupos += 1
    #                 elif semestre > materias[codigo]['MaxSemestre'] and materias[codigo]['prioridad'] == 0:
    #                     cupos += 1  ## 1* cupo en el caso que el estudiante supera el maxsemestre de la materia pero no es prioritaria
    #                 elif semestre <= materias[codigo]['MaxSemestre'] and materias[codigo]['prioridad'] == 0:
    #                     cupos += 1  ## 1? cupo en el caso que el estudiante no supera el maxsemestre de la materia pero no es prioritaria (menor peso que las otras 2 )
    #                 elif semestre < materias[codigo]['MaxSemestre'] and materias[codigo]['prioridad'] == 1:
    #                     cupos += 1  ## 1| cupo en el caso que el estudiante no supera el maxsemestre de la materia y es prioritaria (mayor peso que las 2 anteriores)
    #     return cupos
    #
    #
    # def contrasteHistorico(codigo, cantidad, historico):
    #     promedio = 0
    #     arregloHistoricos = []
    #     for periodo in historico:
    #         cuposHistoricoCanc = 0
    #         cuposHistoricoInsc = 0
    #         grupos = []
    #         for materia in periodo['Materias']:
    #             l = list(materia.keys())
    #             if codigo in l:
    #                 # grupos=obtenerGruposPeriodo(periodo,codigo,grupos)
    #                 grupos.append(materia[l[0]])
    #         for gr in grupos:
    #             cuposHistoricoCanc += gr['Cancelaciones']
    #             cuposHistoricoInsc += gr['Inscritos']
    #         arregloHistoricos.append((cuposHistoricoCanc, cuposHistoricoInsc))
    #         print("\nCupos de la materia " + codigo + " en el periodo " + periodo['periodo'] + " cancelacciones: " + str(
    #             cuposHistoricoCanc) + " inscritos: " + str(cuposHistoricoInsc))
    #         promedio = promediar(cantidad, arregloHistoricos)



    # def porcentajeCancelacionesXMateria(historico):
    #     datosMateriaP1 = cancelacionesPeriodo(historico[0])
    #     datosMateriaP2 = cancelacionesPeriodo(historico[1])
    #     datosMateriaP3 = cancelacionesPeriodo(historico[2])
    #     datosMateriaP4 = cancelacionesPeriodo(historico[3])
    #
    #     procentaje = promedioCancelacionesMateria(datosMateriaP1, datosMateriaP2, datosMateriaP3, datosMateriaP4)
    #
    #
    # def promedioCancelacionesMateria(p1, p2, p3, p4):
    #     porcentajeTotal = {}
    #     for m in p1:
    #         if m not in porcentajeTotal:
    #             porcentajeTotal[m] = p1[m]
    #
    #     for m in p2:
    #         if m not in porcentajeTotal:
    #             porcentajeTotal[m] = p2[m]
    #         else:
    #             porcentajeTotal[m] = (p2[m][0] + porcentajeTotal[m][0], p2[m][1] + porcentajeTotal[m][1])
    #
    #     for m in p3:
    #         if m not in porcentajeTotal:
    #             porcentajeTotal[m] = p3[m]
    #         else:
    #             porcentajeTotal[m] = (p3[m][0] + porcentajeTotal[m][0], p3[m][1] + porcentajeTotal[m][1])
    #
    #     for m in p4:
    #         if m not in porcentajeTotal:
    #             porcentajeTotal[m] = p4[m]
    #         else:
    #             porcentajeTotal[m] = (p4[m][0] + porcentajeTotal[m][0], p4[m][1] + porcentajeTotal[m][1])
    #
    #     # print("Porcentaje Total")
    #     for m in porcentajeTotal:
    #         c = porcentajeTotal[m][0] * 100 / porcentajeTotal[m][1]
    #         # print("Materia " + m + " Inscritos " + str(porcentajeTotal[m][1]) + " Cancelaciones " + str(porcentajeTotal[m][0]) + " porcentaje cancelacion " + str(c))
    #         porcentajeTotal[m] = c
    #     return porcentajeTotal
    #
    #
    # def cancelacionesPeriodo(periodo):
    #     datosM = {}
    #     listaMaterias = periodo['Materias']
    #     for m in listaMaterias:
    #         for c in m.keys():
    #             if c not in datosM:
    #                 datosM[c] = (m[c]['Cancelaciones'], m[c]['Inscritos'])
    #             else:
    #                 datosM[c] = (datosM[c][0] + m[c]['Cancelaciones'], datosM[c][1] + m[c]['Inscritos'])
    #
    #     return datosM
