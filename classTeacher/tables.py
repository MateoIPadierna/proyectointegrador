import django_tables2 as tables
from .models import Proyeccion


class ProyeccionTable(tables.Table):
    class Meta:
        model = Proyeccion
        fields = [
            'materia',
            'cantidad_grupos',
            'cantidad_estudiantes',
            'promedio_historico',
            'periodo',
        ]
        template_name = 'django_tables2/bootstrap.html'
