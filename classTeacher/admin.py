"""Module for administration configuration classTeacher app"""
from django.contrib import admin
from .models import *

# Register your models here.


class MateriaAdmin(admin.ModelAdmin):
    """docstring for MateriaAdmin"""
    list_display = ('codigo', 'nombre', 'creditos', 'cupo')
    list_display_links = ('codigo', 'nombre')


class ProfesorAdmin(admin.ModelAdmin):
    """docstring for ProfesorAdmin"""
    list_display = ('codigo', 'nombre', 'apellido', 'correo', 'carga')
    list_display_links = ('nombre', 'apellido', 'correo')


class PrerrequisitoAdmin(admin.ModelAdmin):
    """docstring for PrerrequisitoAdmin"""
    list_display = ('materia', 'obtener_prerrequisitos')
    list_display_links = ('materia', 'obtener_prerrequisitos')


class ClaseAdmin(admin.ModelAdmin):
    """docstring for ClaseAdmin"""
    list_display = ('profesor', 'materia', 'grupo', 'duracion', 'horario')
    list_display_links = ('profesor', 'materia', 'duracion', 'grupo',
                          'horario')


class ClasesProfesorAdmin(admin.ModelAdmin):
    """docstring for ClaseAdmin"""
    list_display = ('profesor', 'materia')
    list_display_links = ('profesor', 'materia')


admin.site.register(Materia, MateriaAdmin)
admin.site.register(MateriasDisponibles)
admin.site.register(HistorialMaterias)
admin.site.register(Prerrequisito, PrerrequisitoAdmin)
admin.site.register(Profesor, ProfesorAdmin)
admin.site.register(Clase, ClaseAdmin)
admin.site.register(Proyeccion)
admin.site.register(ClasesProfesor, ClasesProfesorAdmin)
