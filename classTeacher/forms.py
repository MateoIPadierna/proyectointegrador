from django import forms
from .models import Profesor, Materia

class ProfesorForm(forms.ModelForm):
    class Meta:
        model = Profesor
        fields = (
            'codigo', 'nombre', 'apellido', 'correo', 'carga',
            'restricciones_horario'
        )


class materiaForm(forms.ModelForm):
    class Meta:
        model = Materia
        fields = (
            'codigo', 'nombre', 'creditos', 'semestre', 'prioridad',
            'cupo', 'departamento', 'programa', 'horas_semanales',
            'horas_presenciales', 'semanas', 'componente','area'
        )

