"""Module url configuration for the classTeacher app"""
from django.urls import path
from . import views

app_name = 'classTeacher'
urlpatterns = [
    path('', views.index, name='index'),
    path('json/WSmxe/', views.wsMateriasEstudiante, name='materias_estudiante'),
    path('json/WShxm/', views.wsHistorialMaterias, name='historial_materias'),
    path('json/WSinfod', views.wsInfoDocentes, name='info_docentes'),
    path('chartComponentes/', views.chartComp, name='chartComp'),
    path('chartProfesores/', views.chart, name='chart'),
    path('profesor_list/', views.profesor_list, name='profe_list'),
    path('profesor/create/', views.profe_create, name='profe_create'),
    path('profesor/<int:pk>/update/', views.profe_update, name='profe_update'),
    path('profesor/<int:pk>/delete/', views.profe_delete, name='profe_delete'),
    path('materia_list/', views.materia_list, name='materia_list'),
    path('materia/create/', views.materia_create, name='materia_create'),
    path('materia/<str:pk>/update/', views.materia_update, name='materia_update'),
    path('materia/<str:pk>/delete/', views.materia_delete, name='materia_delete'),
    path('proyeccion_table/', views.proyeccion_table, name='proyeccion'),
    path('dicts/', views.student, name='dictionary'),
    path('export/xls/', views.export_proyeccion_xls, name='export_proyeccion_xls'),
]
