"""Module for models configuration classTeacher app"""
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator

# Create your models here.


class Materia(models.Model):
    """Clase que modela la información básica de las
    materias."""
    codigo = models.CharField(
        primary_key=True,
        max_length=20,
        validators=[RegexValidator(regex=r'([A-Za-z0-9])+')])
    nombre = models.CharField(
        max_length=100,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    creditos = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(10)])
    semestre = models.CharField(
        max_length=9, validators=[RegexValidator(regex=r'(\d|-\d{1,2}|\+)')])
    prioridad = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        validators=[MinValueValidator(0),
                    MaxValueValidator(1)])
    cupo = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(50)])
    departamento = models.CharField(
        max_length=100,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    programa = models.CharField(
        max_length=100,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    horas_semanales = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        validators=[MinValueValidator(1),
                    MaxValueValidator(15)])
    horas_presenciales = models.IntegerField(
        validators=[MinValueValidator(16),
                    MaxValueValidator(768)])
    semanas = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(30)])
    componente = models.CharField(
        max_length=200,
        default='cualquier componente',
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    area = models.CharField(
        max_length=200,
        default='cualquier cosa',
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])

    def __str__(self):
        return str(self.codigo + " - " + self.nombre)


class MateriasDisponibles(models.Model):
    """Clase que modela el objeto JSON de las materias
    disponibles por estudiante."""
    estudiante = models.IntegerField(validators=[
        MinValueValidator(1000000000),
        MaxValueValidator(9999999999)
    ])
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    veces_cursada = models.IntegerField(
        validators=[MinValueValidator(0),
                    MaxValueValidator(10)])


class Profesor(models.Model):
    """docstring for Profesor"""
    codigo = models.IntegerField(
        primary_key=True,
        validators=[MinValueValidator(100000),
                    MaxValueValidator(9999999999)])
    nombre = models.CharField(
        max_length=30,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    apellido = models.CharField(
        max_length=30,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    correo = models.EmailField(max_length=40)
    carga = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(99)])
    restricciones_horario = models.CharField(
        default=';',
        max_length=50,
        validators=[RegexValidator(regex=r'\w+(\s)\d+-\d|;')])

    def __str__(self):
        return str(self.nombre + " " + self.apellido)


class HistorialMaterias(models.Model):
    """docstring for HistorialMateria"""
    periodo = models.CharField(
        max_length=50,
        validators=[RegexValidator(regex=r'[0-9][0-9][0-9][0-9]-[1-9]')])
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    profesor = models.CharField(
        max_length=50,
        validators=[RegexValidator(regex=r'[a-zA-z]+|[a-zA-z]+\s')])
    grupo = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(20)])
    inscritos = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(50)])
    cancelaciones = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(50)])
    horario = models.CharField(
        # max_length=50, validators=[RegexValidator(regex=r'\w+(\s)\d+-\d|;')])
        max_length=50,
        validators=[RegexValidator(regex=r'(\[\w+\s\d+-\d\s\w+\s\w+\])+')])

    class Meta:
        unique_together = ('periodo', 'materia', 'profesor', 'grupo')


class Prerrequisito(models.Model):
    """docstring for prerrequisito"""
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    prerrequisitos = models.ManyToManyField(
        Materia, related_name='+', symmetrical=False)

    def obtener_prerrequisitos(self):
        """Metodo para retornar el nombre de los prerrequisitos"""
        return ",".join(
            [str(requisito.nombre) for requisito in self.prerrequisitos.all()])

    obtener_prerrequisitos.short_description = 'Prerrequistos'


class Clase(models.Model):
    """docstring for Clase"""
    profesor = models.ForeignKey(Profesor, on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    grupo = models.IntegerField(
        validators=[MinValueValidator(0),
                    MaxValueValidator(20)], default=0)
    duracion = models.IntegerField(
        validators=[MinValueValidator(0),
                    MaxValueValidator(5)], default=0)
    horario = models.CharField(
        max_length=50,
        validators=[RegexValidator(regex=r'\w+(\s)\d+-\d|;')],
        default=';')

    def __str__(self):
        return str("Materia: " + self.materia.nombre + " Profesor: " +
                   self.profesor.nombre + " Grupo: " + str(self.grupo) +
                   " Horario: " + self.horario)


class Proyeccion(models.Model):
    """docstring for proyeccion"""
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    cantidad_grupos = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(20)])
    cantidad_estudiantes = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(50)])
    promedio_historico = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(50)])
    periodo = models.CharField(
        max_length=50,
        validators=[RegexValidator(regex=r'[0-9][0-9][0-9][0-9]-[1-9]')])

    class Meta:
        unique_together = ('periodo', 'materia')

    def __str__(self):
        return str("Materia: " + self.materia.nombre + " Grupos: " +
                   str(self.cantidad_grupos) + " Estudiantes: " +
                   str(self.cantidad_estudiantes) + " Historico: " +
                   str(self.promedio_historico))


class ClasesProfesor(models.Model):
    """docstring for ClasesProfesor"""
    profesor = models.ForeignKey(Profesor, on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)

    def __str__(self):
        return str("Profesor: " + self.profesor.nombre + " " +
                   self.profesor.apellido + "Materia: " + self.materia.nombre)
