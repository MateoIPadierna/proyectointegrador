"""Pygal Module"""
import pygal  # First import pygal
from pygal.style import DefaultStyle
from django.db.models import Sum
from .models import Profesor, Materia, Proyeccion


def profesoresChart():
    """Gráfico de la carga académica de los profesores"""
    data = get_data_Profesores()
    bar_chart = pygal.HorizontalBar(
        style=DefaultStyle)  # Then create a bar graph object
    bar_chart.title = 'Posibles horas por profesor'
    for key, value in data.items():
        bar_chart.add(key, value)

    chart = bar_chart.render_data_uri()  # Render in browser
    return chart


def get_data_Profesores():
    """Obtiene la carga académica de los profesores de la BD"""
    data = {}
    # orden la informacion en orden descendente
    for profe in Profesor.objects.all().order_by('-carga'):
        data[profe.nombre + " " + profe.apellido] = profe.carga
    return data


def componentesChart():
    """Gráfica del número de estudiantes por componente de académico
    pie = pygal.Pie(style=DefaultStyle)
    pie.title = 'Estudiantes Repartidos por componentes'
    for key, value in data.items():
        pie.add(key, data[1])
    piecito = pie.render_data_uri()
    return piecito
    """
    data_componentes = get_data_Componentes()
    # print("Componentes materias: " + str(data_componentes))
    # data = {
    #     "formación profesional": 400,
    #     "formación en ciencias": 200,
    #     "formación general": 350,
    #     "profundización": 100
    # }
    pie = pygal.Pie(style=DefaultStyle)
    pie.title = 'Estudiantes Repartidos por componentes'
    for key, value in data_componentes.items():
        pie.add(key, value)
    piecito = pie.render_data_uri()
    return piecito


def get_data_Componentes():
    """Obtiene la cantidad de cupos por componente de la tabla proyección"""
    data = {}
    componentes = list(Materia.objects.values('componente').distinct())
    # print(str(componentes[0].keys()))
    for componente in componentes:
        suma = {}
        for comp in componente.values():
            # print("Componentes: " + str(comp))
            suma = Proyeccion.objects.filter(
                materia__componente=comp).aggregate(
                    sum=Sum('cantidad_estudiantes'))
            data[componente['componente']] = suma['sum']
    # print("Data: " + str(data))
    return data
