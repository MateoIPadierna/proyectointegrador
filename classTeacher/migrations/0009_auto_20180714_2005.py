# Generated by Django 2.0.4 on 2018-07-15 01:05

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classTeacher', '0008_clasesprofesor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clase',
            name='duracion',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5)]),
        ),
        migrations.AlterField(
            model_name='clase',
            name='grupo',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(20)]),
        ),
        migrations.AlterField(
            model_name='clase',
            name='horario',
            field=models.CharField(default=';', max_length=50, validators=[django.core.validators.RegexValidator(regex='\\w+(\\s)\\d+-\\d|;')]),
        ),
    ]
