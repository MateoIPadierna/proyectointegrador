"""module views for the app classTeacher"""
from django.shortcuts import render
from .pygalChart import testChart

# Create your views here.


def index(request):
    """index classTeacher app"""
    # procesar_info()
    # linea q inicia el flujo del programa cuando se hace peticion al index
    return render(request, 'classTeacher/index.html', {})


def chart(request):
    """Chart view"""
    return render(request, 'classTeacher/chart.html', {'chart': testChart()})
