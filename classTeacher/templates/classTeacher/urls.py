"""Module url configuration for the classTeacher app"""
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('chart/', views.chart, name='chart'),
]
