"""module views for the app classTeacher"""
import os
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.forms import ModelForm
from django.views.decorators.csrf import csrf_exempt
from .utilidades import *
from .proyeccion import *
from .confInicial import *
from .graficos import profesoresChart, componentesChart
from .models import Profesor, Materia, Proyeccion
from django_tables2 import RequestConfig
from .tables import ProyeccionTable
import xlwt
from django.http import HttpResponse
from .forms import ProfesorForm, materiaForm

# Create your views here.


def index(request):

    # peticionDatosWS()
    #iniciarProyeccion([], [])
    iniciarConf()
    return render(request, 'classTeacher/index.html', {})


@csrf_exempt
def wsMateriasEstudiante(request):

    pwd = os.path.dirname(__file__)
    archivo = open(pwd +
                   '/static/classTeacher/JSONData/materiasDisponibles.json')
    # 2 archivos json con la mism info diferente estructura

    data1 = json.load(archivo)
    dataJSON = json.dumps(data1)
    archivo.close()
    # print(dataJSON)
    # data = {"materiasEst": dataJSON}

    return JsonResponse(dataJSON, safe=False)


def wsHistorialMaterias(request):
    pwd = os.path.dirname(__file__)
    archivo = open(pwd + '/static/classTeacher/JSONData/Historico.json')

    data1 = json.load(archivo)
    dataJSON = json.dumps(data1)
    archivo.close()
    # print(dataJSON)
    data = {"historicoMateria": dataJSON}
    return JsonResponse(dataJSON, safe=False)


def wsInfoDocentes(request):
    pwd = os.path.dirname(__file__)
    archivo = open(pwd + '/static/classTeacher/JSONData/Profesores.json')

    data1 = json.load(archivo)
    dataJSON = json.dumps(data1)
    archivo.close()
    # print(dataJSON)
    data = {"infoDoc": dataJSON}
    return JsonResponse(dataJSON, safe=False)


def chart(request):
    """Chart view"""
    return render(request, 'classTeacher/chartProfesores.html',
                  {'chart': profesoresChart()})


def chartComp(request):
    """Chart view"""
    return render(request, 'classTeacher/chartComponentes.html',
                  {'chartComp': componentesChart()})


def proyeccion_table(request):
    proyeccion = ProyeccionTable(Proyeccion.objects.all())
    RequestConfig(request).configure(proyeccion)
    return render(request, 'classTeacher/proyeccion_table.html', {'proyeccion': proyeccion})


def export_proyeccion_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="proyeccion.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Proyeccion')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['Materia', 'Cantidad grupos', 'Cantidad Estudiantes', 'Promedio Historico','Periodo' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Proyeccion.objects.all().values_list('materia', 'cantidad_grupos', 'cantidad_estudiantes', 'promedio_historico','periodo')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

# ------------------Empieza la parte de CRUD profesores

def profesor_list(request, template_name='classTeacher/profesor_list.html'):
    profe = Profesor.objects.all()
    data = {}
    data['profesores'] = profe
    return render(request, template_name, data)

def guardar_formulario(request, form, template):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            profesores = Profesor.objects.all()
            data['html_profesor_list'] = render_to_string('classTeacher/profe_parcial_list.html', {
                'profesores': profesores
            })
        else:
            data['form_is_valid'] = False    
    context = {'form': form}
    data['html_form'] = render_to_string(template, context, request=request)
    return JsonResponse(data)

def profe_create(request):
    if request.method == 'POST':
        form = ProfesorForm(request.POST)
    else:
        form = ProfesorForm()
    return guardar_formulario(request, form, 'classTeacher/profe_parcial_create.html')


def profe_update(request, pk):
    profesor = get_object_or_404(Profesor, pk=pk)
    if request.method == 'POST':
        form = ProfesorForm(request.POST, instance=profesor)
    else:
        form = ProfesorForm(instance=profesor)
    return guardar_formulario(request, form, 'classTeacher/profe_parcial_update.html')


def profe_delete(request, pk):
    profesor = get_object_or_404(Profesor, pk=pk)
    data = dict()
    if request.method == 'POST':
        profesor.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        profesores = Profesor.objects.all()
        data['html_profesor_list'] = render_to_string('classTeacher/profe_parcial_list.html', {
            'profesores': profesores
        })
    else:
        context = {'profesor': profesor}
        data['html_form'] = render_to_string('classTeacher/profe_parcial_delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)

# ------------------Empieza la parte de CRUD Materias
def materia_list(request, template_name='classTeacher/materia_list.html'):
    mate = Materia.objects.all()
    data = {}
    data['materias'] = mate
    return render(request, template_name, data)

def guardar_formulario_materias(request, form, template):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            materias = Materia.objects.all()
            data['html_materia_list'] = render_to_string('classTeacher/materia_parcial_list.html', {
                'materias': materias
            })
        else:
            data['form_is_valid'] = False    
    context = {'form': form}
    data['html_form'] = render_to_string(template, context, request=request)
    return JsonResponse(data)

def materia_create(request):
    if request.method == 'POST':
        form = materiaForm(request.POST)
    else:
        form = materiaForm()
    return guardar_formulario_materias(request, form, 'classTeacher/materia_parcial_create.html')


def materia_update(request, pk):
    materia = get_object_or_404(Materia, pk=pk)
    if request.method == 'POST':
        form = materiaForm(request.POST, instance=materia)
    else:
        form = materiaForm(instance=materia)
    return guardar_formulario_materias(request, form, 'classTeacher/materia_parcial_update.html')


def materia_delete(request, pk):
    materia = get_object_or_404(Materia, pk=pk)
    data = dict()
    if request.method == 'POST':
        materia.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        materias = Materia.objects.all()
        data['html_materia_list'] = render_to_string('classTeacher/materia_parcial_list.html', {
            'materias': materias
        })
    else:
        context = {'materia': materia}
        data['html_form'] = render_to_string('classTeacher/materia_parcial_delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)



#----------------prueba web service------------
def student(request, template_name='classTeacher/dicts.html'):
    dicts = peticionsita(1701315087)
    return render(request, template_name, {'dictionary': dicts})
