from .proyeccion import *
from .models import Materia, Profesor


def iniciarConf():
    criterios()


def criterios():
    ordMateriasPrioritarias()
    print()
    print("______")
    print()
    ordMateriasProfesor()


def getSem(t):
    return t[1]


def getPrio(m):
    return m[2]


def ordMateriasPrioritarias():
    s = [0] * 11
    estXSemestre = []
    infoEst = obtenerListadoEstudiantes()
    for e in infoEst:
        s[e['Semestre']] += 1

    for i in range(1, len(s)):
        estXSemestre.append((i, s[i]))

    estXSemestre = sorted(estXSemestre, key=getSem)
    print(str(estXSemestre))
    materiasBD = Materia.objects.all().values_list('codigo', 'semestre',
                                                   'prioridad')
    for i in reversed(estXSemestre):
        ms = obtenerMateriasSemestre(materiasBD, i[0])
        ms = sorted(ms, key=getPrio)
        ms = list(reversed(ms))
        print("S : "+str(i[0])+" "+str(ms))


def obtenerMateriasSemestre(materiasBD, sem):
    materiasXsem = []
    for i in materiasBD:
        if len(i[1]) == 1:
            s = int(i[1])
            if s == sem:
                materiasXsem.append((i[0], i[1], float(i[2])))
        else:
            if len(i[1]) == 2:
                # print("S : "+str(i))
                pass

            if len(i[1]) == 3:
                s = i[1].split('-')
                if int(s[0]) == sem or int(s[1]) == sem:
                    materiasXsem.append((i[0], i[1], float(i[2])))

    return materiasXsem


def getNR(r):
    return len(r[1])


def ordMateriasProfesor():
    rProfe = Profesor.objects.all().values_list('codigo',
                                                'restricciones_horario')
    profesRestr = []
    for i in range(len(rProfe)):
        profesRestr.append((rProfe[i][0], splitRestricciones(rProfe[i][1])))

    profesRestr = sorted(profesRestr, key=getNR)
    for p in reversed(profesRestr):
        print(str(p))


def splitRestricciones(r):
    return r.split(';')
